<?php 
	include_once './config.php';
	include './pdo.php';
  $sql='select * from chude';
  $objStatament = $objPDO->prepare($sql);
  $objStatament->execute();
  $n = $objStatament->rowCount();
  $chude = $objStatament->fetchAll(PDO::FETCH_OBJ);

  $id = isset($_GET['id'])?$_GET['id']:'';
  $sql='select * from post where id = ?';
  $a = [$id];
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute($a);
  $post = $objStatement->fetchAll(PDO::FETCH_OBJ);

  $sql='select * from category_post where id_post = ?';
 
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute($a);
  $dsPost = $objStatement->fetchAll(PDO::FETCH_OBJ);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Blog</title>
    
</head>
<body style="margin-right: 10px;margin-left: 10px; margin-top:5px; margin-bottom:5px">
<?php include('./page_sub/nav.php') ?>

<div class=" danhsach table-responsive">
      <table class="mx-auto" style="width:400px; " >
       
        <?php 
            foreach($post as $p)
            {
        ?>
            <tr>
                <td  style=" text-align: center; font-size: 50px"><?php echo $p->title ?></td>
            </tr>
            <tr>
            <td style=" text-align: center"><img src="../resources/image/<?php echo $p->image ?>" width="300px" ></td>
            </tr>
            <tr>
              <td style=" text-align: center; font-size: 30px">
                <b>Chủ đề: </b>
                <?php 
                  foreach($chude as $cd)
                    {
                      foreach($dsPost as $ds)
                      {
                        if($ds->id_chude == $cd->id)
                        {
                          ?>

                          <a>
                            <?= $cd->title?>, 
                          </a>
                          <?php
                        }
                      }
                    }
                ?>
               
              </td>
              
            </tr>
            <tr >
                <td style=" text-align: center; font-size: 20px; "><?php echo $p->content ?></td>
            </tr>
            
        <?php
            }
        ?>
    
      </table>
      <br>
    </div>

    <?php include('./page_sub/footer.php')?>
</body>
</html>