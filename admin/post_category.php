<?php 
	include_once './config.php';
	include './pdo.php';
  $sql='select * from chude';
  $objStatament = $objPDO->prepare($sql);
  $objStatament->execute();
  $n = $objStatament->rowCount();
  $chude = $objStatament->fetchAll(PDO::FETCH_OBJ);

  $sql='select * from post';
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute();
  $post = $objStatement->fetchAll(PDO::FETCH_OBJ);
  
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   
    <title>Blog</title>
    
    
</head>
<body style="margin-right: 10px;margin-left: 10px; margin-top:5px; margin-bottom:5px;">
<?php include('./page_sub/nav.php') ?>
<br>
<div><a href="insert_post.php"type="button" class="btn btn-primary">Thêm bài viết</a></div>
<br>
    <div class="danhsach  row" style="margin: auto;">
    <?php 
            foreach($post as $p)
            {
        ?>
                        <div style="margin: auto;">
                            <div class="card mb-4 product-wap rounded-0" style=" width: 400px; height: 500px; margin: auto;" >
                                <div class="card rounded-0" >
                                   <a href="chitiet_post.php?id=<?= $p->id ?>"> <img class="card-img rounded-0 img-fluid" src="../resources/image/<?= $p->image ?> " style=" width: 400px; height: 250px;"></a>
                                    
                                </div>
                                <div class="card-body">
                                    <a href="chitiet_post.php?id=<?= $p->id ?>"><p class="text-center mb-0" style="font-size:30px"><?= $p->title ?></p></a>
                                    <p class="text-center mb-0"><?php echo substr($p->content,0,30) ?>...</p>
                                </div>
                                <div class="card-body">
                                <a href="edit_post.php?id=<?= $p->id ?>"type="button" class="btn btn-primary" style="float: left">Sửa</a>
                                <a href="delete_post.php?id=<?= $p->id ?>&&image=<?= $p->image ?>" type="button" class="btn btn-danger" style="float: right" onclick="return confirm('Bạn có chắc muốn xóa bài viết <?php echo $p->title ?> ? ')" title="delete">Xóa</a>
                                </div>
                            </div>
                        </div>
               
                <?php
            }
        ?> </div>
</div>
<?php include('./page_sub/footer.php')?>
    
</body>
</html>