
<?php 
	 include_once './config.php';
	include './pdo.php';
  $sql='select * from chude';
  $objStatament = $objPDO->prepare($sql);
  $objStatament->execute();
  $n = $objStatament->rowCount();
  $data = $objStatament->fetchAll(PDO::FETCH_OBJ);

  $id = isset($_GET['id'])?$_GET['id']:'';
  $sql='select * from post where id = ?';
  $a = [$id];
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute($a);
  $post = $objStatement->fetchAll(PDO::FETCH_OBJ);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Blog</title>
</head>
<body style="margin-right: 10px;margin-left: 10px; margin-top: 5px; margin-bottom: 5px;">
<?php include('./page_sub/nav.php') ?>
<br>
<div class="danhsach">
<form  action="edit.php" method="post" enctype="multipart/form-data">
    <?php
        foreach ($post as $p)
        {
            ?>

<input type="hidden" name="id" value="<?php echo $p->id ?>" >
  <div class="form-group">
    <label >Title</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" name="title_post" value="<?php echo $p->title ?>">
  </div>
  <div class="form-group">
    <label >Link</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" name="link_post" value="<?php echo $p->link ?>">
  </div>
  <div class="form-group">
    <label>Content</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="content_post" value="<?php echo $p->content ?>"><?php echo $p->content ?></textarea>
  </div>
  <div class="form-group">
    <label>Chủ đề</label><br>
    <?php 
            foreach($data as $r)
            {
                ?>
                 <input  type="checkbox" name="group[]" value="<?php echo $r->id ?>">
                  <label ><?php echo $r->title ?></label>
                <?php
            }
            ?>
  </div>
  <div class="form-group">
    <label for="formFileDisabled" class="form-label">Image</label>
    <input class="form-control" type="file" id="formFileDisabled" name="image_post" >
  </div>
  <input type="hidden" name="image_old" value="<?php echo $p->image ?>">
  <div class="form-group">
  <input type="submit" name="sm" value="Edit" class="btn btn-danger">
  </div>
  <?php
            }
            ?>
</form>
</div>
<?php include('./page_sub/footer.php')?>
</body>
</html>