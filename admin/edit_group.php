
<?php 
	 include_once './config.php';
	include './pdo.php';

  $id = isset($_POST['id'])?$_POST['id']:'';
  $sql='select * from chude where id = ?';
  $a = [$id];
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute($a);
  $chude = $objStatement->fetchAll(PDO::FETCH_OBJ);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Blog</title>
</head>
<body style="margin-right: 10px;margin-left: 10px; margin-top:5px; margin-bottom:5px">
<?php include('./page_sub/nav.php') ?>
<br>
<div class="danhsach">
<form  action="edit_g.php" method="post" enctype="multipart/form-data">
    <?php
        foreach ($chude as $g)
        {
            ?>

<input type="hidden" name="id" value="<?php echo $g->id ?>" >
  <div class="form-group">
    <label >Title</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" name="title_group" value="<?php echo $g->title ?>">
  </div>
  <div class="form-group">
    <label>Link</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Link" name="link_group" value="<?php echo $g->link ?>">
  </div>
  <div class="form-group">
    <label>Content</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="content_group" value="<?php echo $g->content ?>"><?php echo $g->content ?></textarea>
  </div>
  <div class="form-group">
    <label for="formFileDisabled" class="form-label">Image</label>
    <input class="form-control" type="file" id="formFileDisabled" name="image_group" >
  </div>
  <input type="hidden" name="image_old" value="<?php echo $g->image ?>">
  <div class="form-group">
  <input type="submit" name="sm" value="Edit" class="btn btn-danger">
  </div>
  <?php
            }
            ?>
</form>
</div>
<?php include('./page_sub/footer.php')?>
</body>
</html>