<?php 
	include_once './config.php';
	include './pdo.php';
  $sql='select * from chude';
  $objStatament = $objPDO->prepare($sql);
  $objStatament->execute();
  $n = $objStatament->rowCount();
  $chude = $objStatament->fetchAll(PDO::FETCH_OBJ);

  $sql='select * from post';
  $objStatement= $objPDO->prepare($sql);
  $objStatement->execute();
  $post = $objStatement->fetchAll(PDO::FETCH_OBJ);
	?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Blog</title>
    
</head>
<body style="margin-right: 10px;margin-left: 10px; margin-top: 5px; margin-bottom: 5px;">
<?php include('./page_sub/nav.php') ?>
<br>
<div><a href="insert_group.php"type="button" class="btn btn-primary">Thêm Group</a></div>
<br>
<div class=" danhsach table-responsive">
      <table class="table table-striped b-t b-light">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Content</th>
            <th>Image</th>
            <th>Chi tiết</th>
                      
          </tr>

        </thead>
        <?php 
            foreach($chude as $g)
            {
        ?>
            <tbody>
              <td><?php echo $g->id?></td>
               <td><?php echo $g->title ?></td>
               <td id="content"><?php echo substr($g->content,0,50) ?>...</td>
               <td><img src="../resources/image_group/<?php echo $g->image ?>" width="100px" ></td>
               <td>
                <form  action="chitiet_group.php" method="get" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $g->id?>">
                  <input type="submit" name="sm" value="Bài viết" class="btn btn-danger">
                </form>
                <br>
                <form  action="delete_group.php" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $g->id?>">
                  <input type="submit" name="sm" value="Xóa group" class="btn btn-danger"
                  onclick="return confirm('Bạn có chắc muốn xóa group <?php echo $g->title ?> ? Các bài viết thuộc group cũng sẽ được xóa!')" title="delete">
                </form>
                <br>
                <form  action="edit_group.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $g->id ?>">
                <button type="submit" class="btn btn-danger">Sửa chủ đề</button>
                </form>
               </td>
            </tbody>
        <?php
            }
        ?>
      </table>
      <br>
</div>

<?php include('./page_sub/footer.php')?>
</body>
</html>